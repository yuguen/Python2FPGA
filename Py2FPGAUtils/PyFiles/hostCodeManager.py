from Py2FPGAUtils.PyFiles.utils import get_c_type_from_numba_type, get_c_printf_type, is_array,array_dimensions
from subprocess import call
# from numpy import set_printoptions, nan

counter = 0

class HostCodeManager():

	def __init__(self, file_path, args_names, args_types, args_list, return_type, result_in_args, root, arrays_list, report_timing=False):
		self.file_path = file_path
		self.args_names = args_names
		self.args_types = args_types
		self.args_list = args_list
		self.return_type = return_type
		self.host_code = ""
		self.result_in_args = result_in_args
		self.root = root
		self.arrays_list = arrays_list
		self.report_timing = report_timing

	def compile(self, bin_path):
		init_opencl = "source $ALTERAOCLSDKROOT/init_opencl.sh > /dev/null"
		compile_command = "g++ -O2 -fPIC -shared -I./Py2FPGAUtils/CFiles/includes -I"
		compile_command += "$ALTERAOCLSDKROOT/host/include " + self.file_path + " ./Py2FPGAUtils/CFiles/opencl.cpp -L"
		compile_command += "$AOCL_BOARD_PACKAGE_ROOT/linux64/lib -L"
		compile_command += "$ALTERAOCLSDKROOT/host/linux64/lib -Wl,--no-as-needed -lalteracl -lelf -lrt -lpthread -lintel_opae_mmd  -o " + bin_path + " -Wno-ignored-attributes"

		# print (compile_command)

		full_command = init_opencl + " && " + compile_command

		return_code = call(full_command, shell=True)
		assert(return_code == 0)

	def generate_cleanup_function(self):
		self.host_code += "void cleanup() {" + "\n"
		self.host_code += "  if(kernel) clReleaseKernel(kernel);" + "\n"
		self.host_code += "  if(queue) clReleaseCommandQueue(queue);" + "\n"

		for name in self.args_names:
			self.host_code += "  if(input_" + name + "_buf) clReleaseMemObject(input_" + name + "_buf);" + "\n"
		self.host_code += "  if(output_buf) clReleaseMemObject(output_buf);" + "\n"

		self.host_code += "  if(program) clReleaseProgram(program);" + "\n"
		self.host_code += "  if(context) clReleaseContext(context);" + "\n"
		self.host_code += "}" + "\n"
		self.host_code += "\n"

	def generate_code(self):
		self.generate_declarations()
		self.generate_main_function()
		self.generate_init_opencl_function()
		self.generate_init_problem_function()
		self.generate_run_function()
		self.generate_cleanup_function()

		file = open(self.file_path, 'w')
		file.write(self.host_code)
		file.close()

	def generate_declarations(self):
		# Includes
		self.host_code += '#include <stdio.h>' + "\n"
		self.host_code += '#include "CL/opencl.h"' + "\n"
		self.host_code += '#include "aocl_utils.h"' + "\n"
		if(self.report_timing):
			self.host_code += '#include <time.h>' + "\n" 
		self.host_code += "\n"
		
		# OpenCL runtime configuration
		self.host_code += 'using namespace aocl_utils;' + "\n"
		self.host_code += "cl_platform_id platform = NULL;" + "\n"
		self.host_code += "unsigned num_devices = 0;" + "\n"
		self.host_code += "cl_device_id device;" + "\n"
		self.host_code += "cl_context context = NULL;" + "\n"
		self.host_code += "cl_command_queue queue;" + "\n"
		self.host_code += "cl_program program = NULL;" + "\n"
		self.host_code += "cl_kernel kernel;" + "\n"
		self.host_code += "\n"

		types_and_names_and_values = []
		for i in range(len(self.args_names)):
			types_and_names_and_values.append((self.args_types[i], self.args_names[i], self.args_list[i]))

		# Input mem buf declaration
		for arg_type, name, value in types_and_names_and_values:
			self.host_code += "cl_mem input_" + name + "_buf;" + "\n"
			if is_array(str(arg_type)):
				self.host_code += "cl_mem input_arg_" + name + "_0_buf;" + "\n"
				self.host_code += "cl_mem input_arg_" + name + "_1_buf;" + "\n"
				self.host_code += "cl_mem input_" + name + "_no_idea2_buf;" + "\n"
				self.host_code += "cl_mem input_" + name + "_no_idea3_buf;" + "\n"
				dim = array_dimensions(str(arg_type))
				for i in range(dim):
					self.host_code += "cl_mem input_" + name + "_dim1_" + str(i) + "_buf;" + "\n"
					self.host_code += "cl_mem input_" + name + "_dim2_" + str(i) + "_buf;" + "\n"

		self.host_code += "cl_mem output_buf;" + "\n"
		self.host_code += "\n"


		for arg_type, name, value in types_and_names_and_values:
			# self.host_code += "scoped_aligned_ptr<" + get_c_type_from_numba_type(str(arg_type)) + "> " + "input_" + name + ";" + "\n"
			if is_array(str(arg_type)):
				self.host_code +=  get_c_type_from_numba_type(str(arg_type)) + "* " + "input_" + name + ";" + "\n"
				# self.host_code += "scoped_aligned_ptr<char> input_arg_" + name + "_0;" + "\n"
				# self.host_code += "scoped_aligned_ptr<char> input_arg_" + name + "_1;" + "\n"
				# self.host_code += "scoped_aligned_ptr<long> input_" + name + "_no_idea2;" + "\n"
				# self.host_code += "scoped_aligned_ptr<long> input_" + name + "_no_idea3;" + "\n"
				self.host_code += "char input_arg_" + name + "_0[1];" + "\n"
				self.host_code += "char input_arg_" + name + "_1[1];" + "\n"
				self.host_code += "long input_" + name + "_no_idea2[1];" + "\n"
				self.host_code += "long input_" + name + "_no_idea3[1];" + "\n"
				dim = array_dimensions(str(arg_type))
				for i in range(dim):
					self.host_code += "long input_" + name + "_dim1_" + str(i) + "[1];" + "\n"
					self.host_code += "long input_" + name + "_dim2_" + str(i) + "[1];" + "\n"
			else:
				self.host_code +=  get_c_type_from_numba_type(str(arg_type)) + " " + "input_" + name + "[1];" + "\n"

		self.host_code += "" + get_c_type_from_numba_type(self.return_type) + " " + "output[1];" + "\n"
		self.host_code += "\n"


		# Function prototypes
		self.host_code += "bool init_opencl();" + "\n"
		self.host_code += "void init_problem("
		for arg in self.arrays_list:
			self.host_code += "void* " + arg + ", "
		if(len(self.arrays_list) > 0):
			self.host_code = self.host_code[0:len(self.host_code)-2]
		self.host_code += ");" + "\n"
		if(not self.result_in_args):
			self.host_code += get_c_type_from_numba_type(self.return_type) + " run();" + "\n"
		else:
			self.host_code += " void run();" + "\n"

		self.host_code += "void cleanup();" + "\n"
		self.host_code += "\n"

	def generate_init_opencl_function(self):

		self.host_code += "bool init_opencl() {" + "\n"

		if(self.report_timing):
			self.host_code += "  clock_t start_1, end_1;" + "\n" 
			self.host_code += "  double cpu_time;" + "\n"
			self.host_code += "  start_1 = clock();" + "\n"



		self.host_code += "  cl_int status;" + "\n"
		self.host_code += "  if(!setCwdToExeDir()) return false;" + "\n"
		self.host_code += '  platform = findPlatform("Intel");' + "\n"
		self.host_code += "  if(platform == NULL) {" + "\n"
		self.host_code += r'    printf("ERROR: Unable to find Intel FPGA OpenCL platform.\n");' + "\n"
		self.host_code += "    return false;" + "\n"
		self.host_code += "  }" + "\n"
		self.host_code += "  scoped_array<cl_device_id> devices;" + "\n"
		self.host_code += "  devices.reset(getDevices(platform, CL_DEVICE_TYPE_ALL, &num_devices));" + "\n"
		# self.host_code += '  printf("Platform: %s\n", getPlatformName(platform).c_str());" + "\n'
		# self.host_code += '  printf("Using %d device(s)\n", num_devices);' + "\n"
		# self.host_code += "  for(unsigned i = 0; i < num_devices; ++i) {" + "\n"
		# self.host_code += '    printf("  %s\n", getDeviceName(device[i]).c_str());' + "\n"
		# self.host_code += "  }" + "\n"
		self.host_code += "  device = devices[0];" + "\n"
		self.host_code += "  context = clCreateContext(NULL, 1, &device, &oclContextCallback, NULL, &status);" + "\n"
		self.host_code += '  checkError(status, "Failed to create context");' + "\n"
		self.host_code += '  std::string binary_file = getBoardBinaryFile("' + self.root + '/Py2FPGAGenerated", device);' + "\n"
		self.host_code += "  program = createProgramFromBinary(context, binary_file.c_str(), &device, 1);" + "\n"
		self.host_code += '  status = clBuildProgram(program, 0, NULL, "", NULL, NULL);' + "\n"
		self.host_code += '  checkError(status, "Failed to build program");' + "\n"
		# self.host_code += "  queue.reset(1);" + "\n"
		# self.host_code += "  kernel.reset(1);" + "\n"
		self.host_code += "\n"

		# self.host_code += "  n_per_device.reset(1);" + "\n"

		# # Input mem buf reset
		# for name in self.args_names:
		# 	self.host_code += "  input_" + name + "_buf.reset(1);" + "\n"
		# self.host_code += "  output_buf.reset(1);" + "\n"
		# self.host_code += "\n"
		
		self.host_code += "  queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);" + "\n"
		self.host_code += '  checkError(status, "Failed to create command queue");' + "\n"
		self.host_code += '  const char *kernel_name = "pyfunc";' + "\n"
		self.host_code += "\n"

		self.host_code += "  kernel = clCreateKernel(program, kernel_name, &status);" + "\n"
		self.host_code += '  checkError(status, "Failed to create kernel");' + "\n"
		self.host_code += "\n"
		


		if(self.report_timing):
			self.host_code += "  end_1 = clock();" + "\n"
			self.host_code += "  cpu_time = ((double) (end_1 - start_1)) / CLOCKS_PER_SEC;" + "\n"
			self.host_code += r'  printf("(fpga) fetch FPGA : %f seconds\n", cpu_time);' + "\n"


		if(self.report_timing):
			self.host_code += "  start_1 = clock();" + "\n"


		types_and_names_and_values = []
		for i in range(len(self.args_names)):
			types_and_names_and_values.append((self.args_types[i], self.args_names[i], self.args_list[i]))

		# Input mem declaration
		for arg_type, name, value in types_and_names_and_values:
			if is_array(str(arg_type)):
				dim = array_dimensions(str(arg_type))
				size = 1
				tmp_value = value
				for i in range(dim):
					size *= len(tmp_value)
					tmp_value = tmp_value[0]

				c_size = "sizeof(" + get_c_type_from_numba_type(str(arg_type)) + ")*" + str(size)
				c_name = "input_" + name 
				c_buf_name = "input_" + name + "_buf"

				if(name.startswith("fpga_output_")):
					self.res = c_name
					self.res_size = c_size
					self.res_buf = c_buf_name
					self.res_dim = dim
					self.res_value = value
					self.res_type = arg_type
					self.res_nb_elems = size

				self.host_code += "  " + c_buf_name + " = clCreateBuffer(context, CL_MEM_READ_ONLY, " + c_size
				self.host_code += " , NULL, &status);" + "\n"
				self.host_code += '  checkError(status, "Failed to create buffer for input ' + name + '");' + "\n"
				

				self.host_code += "  input_arg_" + name + "_0_buf = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(char), NULL, &status);" + "\n"
				self.host_code += '  checkError(status, "Failed to create buffer for input ' + name + '(_0)");' + "\n"
				self.host_code += "  input_arg_" + name + "_1_buf = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(char), NULL, &status);" + "\n"
				self.host_code += '  checkError(status, "Failed to create buffer for input ' + name + '(_1)");' + "\n"
				self.host_code += "  input_" + name + "_no_idea2_buf = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(long), NULL, &status);" + "\n"
				self.host_code += '  checkError(status, "Failed to create buffer for input ' + name + '(_no_idea2)");' + "\n"
				self.host_code += "  input_" + name + "_no_idea3_buf = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(long), NULL, &status);" + "\n"
				self.host_code += '  checkError(status, "Failed to create buffer for input ' + name + '(_no_idea3)");' + "\n"
				for i in range(dim):
					self.host_code += "  input_" + name + "_dim1_" + str(i) + "_buf = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(long), NULL, &status);" + "\n"
					self.host_code += '  checkError(status, "Failed to create buffer for input ' + name + '(_dim1_' + str(i) + ')");' + "\n"
					self.host_code += "  input_" + name + "_dim2_" + str(i) + "_buf = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(long), NULL, &status);" + "\n"
					self.host_code += '  checkError(status, "Failed to create buffer for input ' + name + '(_dim2_' + str(i) + ')");' + "\n"

			else:
				self.host_code += "  input_" + name + "_buf = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(" + get_c_type_from_numba_type(str(arg_type)) + "), NULL, &status);" + "\n"
				self.host_code += '  checkError(status, "Failed to create buffer for input ' + name + '");' + "\n"

			self.host_code += "\n"

		self.host_code += "  output_buf = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(" + get_c_type_from_numba_type(self.return_type) + "), NULL, &status);" + "\n"
		self.host_code += '  checkError(status, "Failed to create buffer for output");\n'
		self.host_code += "\n"
		
		if(self.report_timing):
			self.host_code += "  end_1 = clock();" + "\n"
			self.host_code += "  cpu_time = ((double) (end_1 - start_1)) / CLOCKS_PER_SEC;" + "\n"
			self.host_code += r'  printf("(fpga) init FPGA mem : %f seconds\n", cpu_time);' + "\n"




		self.host_code += "  return true;" + "\n"
		self.host_code += "}" + "\n"
		self.host_code += "\n"

	def generate_init_problem_function(self):
		# set_printoptions(threshold=nan)
		self.host_code += "void init_problem("
		for arg in self.arrays_list:
			self.host_code += "void* " + arg + ", "
		if(len(self.arrays_list) > 0):
			self.host_code = self.host_code[0:len(self.host_code)-2]
		self.host_code += ") {" + "\n"
		self.host_code += '  if(num_devices == 0) checkError(-1, "No devices");' + "\n"
		self.host_code += "\n"

		# Input mem reset
		types_and_names_and_values = []
		for i in range(len(self.args_names)):
			types_and_names_and_values.append((self.args_types[i], self.args_names[i], self.args_list[i]))

		# for arg_type, name, value in types_and_names_and_values:
		# 	if is_array(str(arg_type)):
		# 		dim = array_dimensions(str(arg_type))
		# 		size = 1
		# 		tmp_value = value
		# 		for i in range(dim):
		# 			size *= len(tmp_value)
		# 			tmp_value = tmp_value[0]
		# 		self.host_code += "  input_" + name + ".reset(" + str(size) + ");" + "\n"


		# 		self.host_code += "  input_arg_" + name + "_0.reset(1);" + "\n"
		# 		self.host_code += "  input_arg_" + name + "_1.reset(1);" + "\n"
		# 		self.host_code += "  input_" + name + "_no_idea2.reset(1);" + "\n"
		# 		self.host_code += "  input_" + name + "_no_idea3.reset(1);" + "\n"
		# 		for i in range(dim):
		# 			self.host_code += "  input_" + name + "_dim1_" + str(i) + ".reset(1);" + "\n"
		# 			self.host_code += "  input_" + name + "_dim2_" + str(i) + ".reset(1);" + "\n"

		# 	else:
		# 		self.host_code += "  input_" + name + ".reset(1);" + "\n"
			

		# self.host_code += "  output.reset(1);" + "\n"
		# self.host_code += "\n"

		for arg_type, name, value in types_and_names_and_values:
			if is_array(str(arg_type)):
				dim = array_dimensions(str(arg_type))
				size = 1
				tmp_value = value
				for i in range(dim):
					size *= len(tmp_value)
					tmp_value = tmp_value[0]

				self.host_code += "  input_arg_" + name + "_0[0] = 0;" + "\n"
				self.host_code += "  input_arg_" + name + "_1[0] = 0;" + "\n"
				self.host_code += "  input_" + name + "_no_idea2[0] = 0;" + "\n"
				self.host_code += "  input_" + name + "_no_idea3[0] = 0;" + "\n"
				current_scope = value
				for i in range(dim):
					self.host_code += "  input_" + name + "_dim1_" + str(i) + "[0] = " + str(len(current_scope)) + ";" + "\n"
					self.host_code += "  input_" + name + "_dim2_" + str(i) + "[0] = 0;" + "\n"
					current_scope = current_scope[0]

				self.host_code += "  input_" + name + " = (" + get_c_type_from_numba_type(str(arg_type)) + "*) " + name + ";" + "\n"


				# formated_value = str(value)
				# formated_value = formated_value.replace("["," ")
				# formated_value = formated_value.replace("]"," ")
				# formated_value_list = formated_value.split()
				
				# c = 0
				# for val in formated_value_list:
				# 	self.host_code += "  input_" + name + "[" + str(c) + "] = " + val + ";" + "\n"
				# 	c += 1
			else:
				self.host_code += "  input_" + name + "[0] = " + str(value) + ";" + "\n"
		
		self.host_code += "}" + "\n"
		self.host_code += "\n"

	def generate_main_function(self):
		if(not self.result_in_args):
			self.host_code += 'extern "C" {\n' + get_c_type_from_numba_type(self.return_type) + ' my_func('
		else:
			self.host_code += 'extern "C" {\n' + 'int my_func('
		for arg in self.arrays_list:
			self.host_code += "void* " + arg + ", "
		if(len(self.arrays_list) > 0):
			self.host_code = self.host_code[0:len(self.host_code)-2]
		self.host_code += ") {" + "\n"

		self.host_code += "  if(!init_opencl()) return -1;" + "\n"
		

		self.host_code += "  init_problem("
		for arg in self.arrays_list:
			self.host_code += arg + ", "
		if(len(self.arrays_list) > 0):
			self.host_code = self.host_code[0:len(self.host_code)-2]
		self.host_code += ");" + "\n"


		if(not self.result_in_args):
			self.host_code += "  " + get_c_type_from_numba_type(self.return_type) + " var = run();" + "\n"
		else:
			self.host_code += "  run();" + "\n"
		self.host_code += "  cleanup();" + "\n"
		if(not self.result_in_args):
			self.host_code += "  return var;" + "\n"
		else:
			self.host_code += "  return 0;" + "\n"

		self.host_code += "}" + "\n"
		self.host_code += '}'
		self.host_code += "\n"
		# self.host_code += 'extern "C" {\n' + 'int test(void* in, void* in2){\n'
		# self.host_code += r' long* new_in = (long*) in;' + '\n'
		# self.host_code += r' long* new_in2 = (long*) in2;' + '\n'
		# self.host_code += r' printf("in : %d\n", new_in[0]);' + '\n'
		# self.host_code += r' printf("in : %d\n", new_in2[1]);' + '\n'
		# self.host_code += 'return 0;	}}' + '\n'


	def generate_run_function(self):
		
		if(not self.result_in_args):
			self.host_code += get_c_type_from_numba_type(self.return_type) + " run() {" + "\n"
		else:
			self.host_code += "void run() {" + "\n"

		self.host_code += "  cl_int status;" + "\n"
		self.host_code += "  cl_event kernel_event;" + "\n"
		self.host_code += "  cl_event finish_event;" + "\n"
		self.host_code += "\n"

		nb_events = 0
		for arg_type in self.args_types:
			nb_events += 1
			if is_array(str(arg_type)):
				nb_events += 4 + array_dimensions(str(arg_type))*2

		self.host_code += "  cl_event write_event[" + str(nb_events) + "];" + "\n"
		self.host_code += "\n"

		types_and_names_and_values = []
		for i in range(len(self.args_names)):
			types_and_names_and_values.append((self.args_types[i], self.args_names[i], self.args_list[i]))

		if(self.report_timing):
			self.host_code += "  clock_t start, end;" + "\n" 
			self.host_code += "  double cpu_time_used;" + "\n"
			self.host_code += "  start = clock();" + "\n"

		i = 0
		for arg_type, name, value in types_and_names_and_values:
			if is_array(str(arg_type)):
				dim = array_dimensions(str(arg_type))
				size = 1
				tmp_value = value
				for k in range(dim):
					size *= len(tmp_value)
					tmp_value = tmp_value[0]


				self.host_code += "  status = clEnqueueWriteBuffer(queue, input_" + name + "_buf, CL_FALSE, 0, sizeof(" + get_c_type_from_numba_type(str(arg_type)) + ")*" + str(size) 
				self.host_code += ", input_" + name + ", 0, NULL, &write_event[" + str(i) + "]);" + "\n"
				self.host_code += '  checkError(status, "Failed to transfer input ' + name + '");' + "\n"
				i += 1

				self.host_code += "  status = clEnqueueWriteBuffer(queue, input_arg_" + name + "_0_buf, CL_FALSE, 0, sizeof(char)" 
				self.host_code += ", input_arg_" + name + "_0, 0, NULL, &write_event[" + str(i) + "]);" + "\n"
				self.host_code += '  checkError(status, "Failed to transfer input ' + name + '(_0)");' + "\n"
				i += 1

				self.host_code += "  status = clEnqueueWriteBuffer(queue, input_arg_" + name + "_1_buf, CL_FALSE, 0, sizeof(char)" 
				self.host_code += ", input_arg_" + name + "_1, 0, NULL, &write_event[" + str(i) + "]);" + "\n"
				self.host_code += '  checkError(status, "Failed to transfer input ' + name + '(_1)");' + "\n"
				i += 1

				self.host_code += "  status = clEnqueueWriteBuffer(queue, input_" + name + "_no_idea2_buf, CL_FALSE, 0, sizeof(long)" 
				self.host_code += ", input_" + name + "_no_idea2, 0, NULL, &write_event[" + str(i) + "]);" + "\n"
				self.host_code += '  checkError(status, "Failed to transfer input ' + name + '(_no_idea2)");' + "\n"
				i += 1

				self.host_code += "  status = clEnqueueWriteBuffer(queue, input_" + name + "_no_idea3_buf, CL_FALSE, 0, sizeof(long)" 
				self.host_code += ", input_" + name + "_no_idea3, 0, NULL, &write_event[" + str(i) + "]);" + "\n"
				self.host_code += '  checkError(status, "Failed to transfer input ' + name + '(_no_idea3)");' + "\n"
				i += 1

				for j in range(dim):
					self.host_code += "  status = clEnqueueWriteBuffer(queue, input_" + name + "_dim1_" + str(j) + "_buf, CL_FALSE, 0, sizeof(long)" 
					self.host_code += ", input_" + name + "_dim1_" + str(j) + ", 0, NULL, &write_event[" + str(i) + "]);" + "\n"
					self.host_code += '  checkError(status, "Failed to transfer input ' + name + '(_dim1_' + str(j) + ')");' + "\n"
					i += 1

					self.host_code += "  status = clEnqueueWriteBuffer(queue, input_" + name + "_dim2_" + str(j) + "_buf, CL_FALSE, 0, sizeof(long)" 
					self.host_code += ", input_" + name + "_dim2_" + str(j) + ", 0, NULL, &write_event[" + str(i) + "]);" + "\n"
					self.host_code += '  checkError(status, "Failed to transfer input ' + name + '(_dim2_' + str(j) + ')");' + "\n"
					i += 1

			else:
				self.host_code += "  status = clEnqueueWriteBuffer(queue, input_" + name + "_buf, CL_FALSE, 0, sizeof(" + get_c_type_from_numba_type(str(arg_type)) + ")" 
				self.host_code += ", input_" + name + ", 0, NULL, &write_event[" + str(i) + "]);" + "\n"
				self.host_code += '  checkError(status, "Failed to transfer input ' + name + '");' + "\n"
				i = i+1
			
			self.host_code += "\n"

		self.host_code += "  unsigned argi = 0;" + "\n"
		self.host_code += "\n"

		# self.host_code += "  input_arg_" + name + "_0 = 0;" + "\n"
		# self.host_code += "  input_arg_" + name + "_1 = 0;" + "\n"
		# self.host_code += "  input_" + name + "_no_idea2 = 0;" + "\n"
		# self.host_code += "  input_" + name + "_no_idea3 = 0;" + "\n"
		# current_scope = value
		# for i in range(dim):
		# 	self.host_code += "  input_" + name + "_dim1_" + str(i) + " = " + str(len(current_scope)) + ";" + "\n"
		# 	self.host_code += "  input_" + name + "_dim2_" + str(i) + " = 0;" + "\n"


		for arg_type, name, value in types_and_names_and_values:
			self.host_code += "  status = clSetKernelArg(kernel, argi++, sizeof(cl_mem), &input_" + name + "_buf);" + "\n"
			self.host_code += '  checkError(status, "Failed to set argument %d", argi - 1);' + "\n"
			if is_array(str(arg_type)):
				dim = array_dimensions(str(arg_type))
				self.host_code += "  status = clSetKernelArg(kernel, argi++, sizeof(cl_mem), &input_arg_" + name + "_0_buf);" + "\n"
				self.host_code += '  checkError(status, "Failed to set argument %d", argi - 1);' + "\n"
				self.host_code += "  status = clSetKernelArg(kernel, argi++, sizeof(cl_mem), &input_arg_" + name + "_1_buf);" + "\n"
				self.host_code += '  checkError(status, "Failed to set argument %d", argi - 1);' + "\n"
				self.host_code += "  status = clSetKernelArg(kernel, argi++, sizeof(cl_mem), &input_" + name + "_no_idea2_buf);" + "\n"
				self.host_code += '  checkError(status, "Failed to set argument %d", argi - 1);' + "\n"
				self.host_code += "  status = clSetKernelArg(kernel, argi++, sizeof(cl_mem), &input_" + name + "_no_idea3_buf);" + "\n"
				self.host_code += '  checkError(status, "Failed to set argument %d", argi - 1);' + "\n"
				
				for j in range(dim):
					self.host_code += "  status = clSetKernelArg(kernel, argi++, sizeof(cl_mem), &input_" + name + "_dim1_" + str(j) +"_buf);" + "\n"
					self.host_code += '  checkError(status, "Failed to set argument %d", argi - 1);' + "\n"
				for j in range(dim):
					self.host_code += "  status = clSetKernelArg(kernel, argi++, sizeof(cl_mem), &input_" + name + "_dim2_" + str(j) +"_buf);" + "\n"
					self.host_code += '  checkError(status, "Failed to set argument %d", argi - 1);' + "\n"


				
			self.host_code += "\n"
		self.host_code += "  status = clSetKernelArg(kernel, argi++, sizeof(cl_mem), &output_buf);" + "\n"
		self.host_code += '  checkError(status, "Failed to set argument %d", argi - 1);' + "\n"
		self.host_code += "\n"
		

		if(self.report_timing):
			for j in range(i-1):
				self.host_code += "  clWaitForEvents(" + str(1) + ", &write_event[" + str(j) + "]);" + "\n"
			self.host_code += "  end = clock();" + "\n"
			self.host_code += "  cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;" + "\n"
			self.host_code += r'  printf("(fpga) Mem copy to FPGA : %f seconds\n", cpu_time_used);' + "\n"

			self.host_code += "  start = clock();" + "\n"


		self.host_code += "  size_t gSize[3] = {1, 1, 1};" + "\n"
		# self.host_code += "  status = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &global_work_size, NULL, 2, write_event, &kernel_event[i]);" + "\n"
		self.host_code += "  status = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, gSize, NULL, 2, write_event, &kernel_event);" + "\n"
		self.host_code += '  checkError(status, "Failed to launch kernel");' + "\n"
		self.host_code += "\n"

		if(self.report_timing):
			self.host_code += "  clWaitForEvents(1, &kernel_event);" + "\n"
			self.host_code += "  end = clock();" + "\n"
			self.host_code += "  cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;" + "\n"
			self.host_code += r'  printf("(fpga) Kernel execution : %f seconds\n", cpu_time_used);' + "\n"

			self.host_code += "  start = clock();" + "\n"

		if(self.result_in_args):
			self.host_code += "  status = clEnqueueReadBuffer(queue, " + self.res_buf + " , CL_FALSE, 0," + self.res_size + ", " + self.res + ", 1, &kernel_event, &finish_event);" + "\n"
		else:
			self.host_code += "  status = clEnqueueReadBuffer(queue, output_buf, CL_FALSE, 0, sizeof(" + get_c_type_from_numba_type(self.return_type) + "), output, 1, &kernel_event, &finish_event);" + "\n"
		self.host_code += '  checkError(status, "Failed to read output");' + "\n"
		self.host_code += "\n"


		
		for j in range(len(self.args_list)):
			self.host_code += "  clReleaseEvent(write_event[" + str(j) + "]);" + "\n"
		self.host_code += "\n"
			
		self.host_code += "  clWaitForEvents(1, &finish_event);" + "\n"

		if(self.report_timing):
			self.host_code += "  end = clock();" + "\n"
			self.host_code += "  cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;" + "\n"
			self.host_code += r'  printf("(fpga) Mem copy to host : %f seconds\n", cpu_time_used);' + "\n"

		self.host_code += "  clReleaseEvent(kernel_event);" + "\n"
		self.host_code += "  clReleaseEvent(finish_event);" + "\n"

		if(self.result_in_args):
			# printf_type = get_c_printf_type(str(self.res_type))
			# self.host_code += '  printf("'
			# def rec(dim, value, ptype):
			# 	# print(dim)
			# 	# print(len(value))
			# 	self.host_code += '['
			# 	for k in range(len(value)):
			# 		if (dim is not 1):
			# 			rec(dim-1, value[0], ptype) 
			# 			if(k is not (len(value)-1)):
			# 				self.host_code += ', '
			# 		else:
			# 			# self.host_code += '['
			# 			if(k is 0):
			# 				for l in range(len(value)-1):
			# 					self.host_code += ptype + ', '
			# 				self.host_code += ptype 
			# 			# self.host_code += ']'

			# 	self.host_code += ']'


			# rec(self.res_dim, self.res_value, printf_type)
			# self.host_code += '", '
			# for k in range(self.res_nb_elems-1):
			# 	self.host_code += ' ' + self.res + '[' + str(k) + '],'
			# self.host_code += ' ' + self.res + '[' + str(self.res_nb_elems-1) + ']);'
			# self.host_code += '\n'

			#ici

			# printf_type = get_c_printf_type(str(self.res_type))

			# global counter
			# counter = 0


			# def rec(dim, value, ptype):
			# 	global counter
			# 	# print(dim)
			# 	# print(len(value))
			# 	self.host_code += 'printf("[");\n'
			# 	for k in range(len(value)):
			# 		if (dim is not 1):
			# 			rec(dim-1, value[0], ptype) 
			# 			if(k is not (len(value)-1)):
			# 				self.host_code += 'printf(",");'
			# 		else:
			# 			# self.host_code += '['
			# 			if(k is 0):
			# 				self.host_code += 'printf("'
			# 				for l in range(len(value)-1):
			# 					self.host_code += ptype + ', '
			# 				self.host_code += ptype
			# 				self.host_code += '", '
			# 				for l in range(len(value)-1):
			# 					self.host_code += self.res + '[' + str(counter) + ']' + ', '
			# 					counter = counter + 1
			# 				self.host_code += self.res + '[' + str(counter) + ']' 
			# 				counter = counter + 1
			# 				self.host_code += ');\n'

			# 			# self.host_code += ']'
			# 	self.host_code += 'printf("]");\n'
			# rec(self.res_dim, self.res_value, printf_type)
			pass

		else:
			self.host_code += '  return output[0];' + "\n"

		self.host_code += "}" + "\n"
		self.host_code += "\n"