from Py2FPGAUtils.PyFiles.utils import get_c_type_from_numba_type, is_array, array_dimensions
from subprocess import call
from os import path, environ
from numba import typeof
from Py2FPGAUtils.PyFiles.perlScriptManager import PerlScriptManager


class CLPlaceholderManager():

	def __init__(self, build_dir, args_names, args_list, return_type, simulation, fast_compile):
		self.build_dir = build_dir
		self.args_names = args_names
		self.args_list = args_list
		self.return_type = return_type
		self.simulation = simulation
		self.fast_compile = fast_compile

		try:
			environ['AOCL_BOARD_PACKAGE_ROOT']
		except KeyError:
			Raise("AOCL_BOARD_PACKAGE_ROOT environment variable is not set.")

	def generate_aoco_file(self):
		perl_script_manager = PerlScriptManager(self.build_dir, "get_aoco.pl", self.simulation)
		perl_script_manager.generate_script()
		perl_script_manager.run_script()

	def get_required_files(self, modified_ll):
		
		ipx_file = open(path.join(self.build_dir, "opencl.ipx"), 'w')
		ipx_file.write('<?xml version="1.0" encoding="UTF-8"?>' + "\n")
		ipx_file.write('<library>' + "\n")
		ipx_file.write(' <path path="${ALTERAOCLSDKROOT}/ip/*" />' + "\n")
		ipx_file.write('</library>' + "\n")
		ipx_file.close()

		ip_include_file = open(path.join(self.build_dir, "ip_include.tcl"), 'w')
		ip_include_file.write('set_global_assignment -name SEARCH_PATH "$::env(ALTERAOCLSDKROOT)/ip"')
		ip_include_file.close()

		# self.aoco_kernel_file_path = path.join(self.build_dir, aoco_kernel_file_path)
		self.ll_to_aoco_scprit_path = path.join(self.build_dir,'ll_to_aoco.sh')
		
		add_cl_clang_to_path_command = "export PATH=$PATH:$ALTERAOCLSDKROOT/linux64/bin"
		add_libs_to_path = "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ALTERAOCLSDKROOT/linux64/lib/dspba/linux64/:$ALTERAOCLSDKROOT/linux64/lib/"
		
		cd_command = "cd " + self.build_dir

		if self.simulation:
			emit_llvm_bc_command = "aocl-clang -cc1 -target-abi opencl -emit-llvm-bc -mllvm -gen-efi-tb -Wuninitialized -triple x86_64-unknown-linux-gnu -mllvm -board -mllvm "
		else:
			emit_llvm_bc_command = "aocl-clang -cc1 -O3 -emit-llvm-bc -Wuninitialized -triple fpga64 -mllvm -board -mllvm "
			
		emit_llvm_bc_command += "$AOCL_BOARD_PACKAGE_ROOT/hardware/pac_a10/board_spec.xml -DACL_BOARD_pac_a10=1 -DAOCL_BOARD_pac_a10=1 "
		emit_llvm_bc_command += modified_ll + " -o " + "Py2FPGAGenerated.pre.bc" + " -g"

		link_command = "aocl-link " + "Py2FPGAGenerated.pre.bc" + " "
		if self.simulation:
			link_command += "$ALTERAOCLSDKROOT/share/lib/acl/acl_emulation.bc " 
		else:
			link_command += "$ALTERAOCLSDKROOT/share/lib/acl/acl_early.bc "
		link_command += "-o Py2FPGAGenerated.1.bc"

		acle_key = "ljg7wk8o12ectgfpthjmnj8xmgf1qb17frkzwewi22etqs0o0cvorlvczrk7mipp8xd3egwiyx713svzw3kmlt8clxdbqoypaxbbyw0oygu1nsyzekh3nt0x0jpsmvypfxguwwdo880qqk8pachqllyc18a7q3wp12j7eqwipxw13swz1bp7tk71wyb3rb17frk3egwiy2e7qjwoe3bkny8xrrdbq1w7ljg70g0o1xlbmupoecdfluu3xxf7l3dogxfs0lvm7jlzqjvo33gclly3xxf7mi8p32dc7udmirekmgvoy1bknyycrgfhmczpyxgf0wvz7jlzmy8p83kfnedxz2azqb17frk77qdiyxlkmh8ithkcluu3xxf7nvyzs2kmegdoyxlctgfptck3nt0318a7mcyz1xgu7uui3rezlg07ekh3lqjxtgdmnczpy2jtehdo3xyctgfpthjmljpbzgdmlb17frkuww0zwreeqapzkhholuu3xxf7nz8p3xguwypp3gw7mju7atjbnhdxmrjumipprxdceg0z880qqk8z2tk7qjjxbxacnzvpfxbbyw0otrebma8z2hdolkwx18a7m8jorxbbyw0o72eona8iekh3nyvb1rzbtijz82hkwhjibgwklgfptchqlyvc7rahm8w7ljg7wldzzxu13svz0cg1mt8c8gssmxw7ljg70tjzwgukmkyo03k7qjjxwgfzmb0ogrgswtfmirezqspo23kfnuwb1rdbtijz3gffwhpom2e3ldjpacvorlvcqgskq10pggju7ryomx713svzkhh3qhvccgammzpplxbbyw0ow2ekmavzuchfntwxzga33czpyrfu0evzp2qmqwvzltk72tfxm2kbmbjo8rg70qpow2wctgfptchhnl0b18a7q8vpm2kc7uvm7jlzma8pt3h72tfxmrafmiwoljg70kyitrykmrvzj3bknywbp2kbm8wpdxgc0uui0x1ctgfptchhnl0b18a7m3pp8rduwk0ovx713svzkhh3qhvccgammzpplxbbyw0obgl3mt8z2td72tfxmrafmiwoljg70qjobrlumju7atjfnljxwgpsmv0zlxgbwkpioglctgfpttkbql0318a7mo8zark37swiyxyctgfpttd3ny0b0jpsmvypfrfc7rwizgekmsyzy1bknypxuga3nczpyxdtwgdo1xwkmsjzy1bknyvcc2kmnc0prxdbwudmirecnujp83jcnuwbzxasqb17frkc0gdo880qqk8zwtjoluu3xxf7nz8p3xguwypp3gw7mju7atjqllvbyxffmodzgggbwtfmireznrpokcdorlvc8gd1qc87frk3egwiwrl3lw0oetd72tfxmxdhmidzrxgc0rdo880qqkvzshh3qhyx12kzmcw7ljg7wrporgukqgpoy1bknyvcc2aoncdom2vs0rpiogu3qgfpt3ghngpb18a7q3vpljg70qyiwgukmsvoktj3quu3xxfhmivolgfbyw0oy2qclgvoy1bknyybx2kfqo0pljg70rvi1glqqgwp3cvorlvc8xdon38zt8vs0rjiorlclgfpttdqnq0bvgsom7w7ljg7we0otrubmju7atjfnljxwgpsmv8ofgffweji880qqk8patdzmyjxb2fuqi8zt8vs0rjokgloruwoucf72tfxmga1m8jpr2k38uui3rukqa0od3gbnfvc2xd33czpyrf70rvz880qqkjznhh72tfxmgabmojz32sfwhjz880qqk8patdzmyjxb2fuqi8zt8vs0r8ie2lclgfptcfeljyc7rduqivzt8vs0rjot2wctgfpt3jznyyc0jpsmvjog2g3eepin2tzmhjzy1bkny8xxxkcnvvpagkuwwdo880qqkvo0hdqlky3xxfcmcyzq2vs0rvzrrl3lswobcdonj0318a7mx0pdgk1wejo32wctgfpttjhllvbygabq8vzlxbbyw0o22eonu8pehd72tfxmgdomz87frk37wpiixy1muy7atjmljjc18a7mbpprxgu7gpitx713svzuchcmu8vqgpsmvypfxguwwdo880qqkdpshdsljycyrzbtijza2jm7ydor2w3lrpoacvorlvcz2acncdoe2k70u0z880qqkwzqhfomt0318a7q1dod2gfwkdotxybmr8pttd72tfxmgf7qo87frkh0wpo020qqkvo0th1mty3xxfuqi0o12hs0gpi3ru13svzuckzluu3xxftmbppqxgb0uui32wmqdvzuchfnj0318a7q3dorgg7eywir2l13svzehkqny8cvra7lb17frkh0wwz7guzqsdpncvorlvc8xfbqb17frkh0wwz7guzqsdp3cf72tfxmgfhm2yzxrfbyw0or2wuqsdoe3bknyyx1gabtijzgggcegpiirukqkvoy1bknypb1gafq28zljg70wwi220qqk8zttjonjvb1xk33czpyxj70uvm7jlzquvzuchmljpb1rkhqb17frkc0rdo880qqk0o23gbqjzb1xd33czpygfb0ewil2w13svzkhhqlh8x7rauq0yzm2hfwwdmireznrjp23k3quu3xxf3l8vpargc7uvzr2qmnju7atjmltvc1rzbtijzsrgfwhdmirezldyp8chqlr8v0jpsmvwoh2hbyw0o3rlbmayz3tjqlljc0jpsmvjog2g3eepin2tzmhjzy1bkny8xxxkcnvvpagkuwwdo880qqkwzt3k72tfxmxdhmidzrxgc0rdo880qqkdzkcaoqky3xxfmmz0oy2k7ek0z880qqkwpf3ksntfxmxk3loypmxdbwgvm7jlzqjwzwhdonjvb18a7m8worrkb0uui3gy1mu8pl3a72tfxm2jbq8ype2s38uui3rykmgwoj3bknyjbzxkbmczpy2gbwgwo7x713svzwtjoluu3xxf1q30o12hm0kjz7xuols0outkmlh8xc2a33czpyrjoewvm7jlzmgjzn3foluu3xxfcmv8zt8vs0r0zb2lcna8pl3a3lrjc0jpsmv0p8xdm7k0ow2qmqw07ekh3nedxqrj7mi8pu2hs0udmireqqsy7atjbquu3xxf7nvyzs2km7q8p720qqkwpuhdcnty3xxf1mxvz12jz0tjz7ructgfpttd3nuwx72kmncwosxbbyw0o0re1mju7atjbne0vurkbtijzu2hc7rpotxyznju7atjfnljb12k7n2ypmrktwtfmirezlk8zd3j1mepv1rzbtijzfrgm7ejitx713svzwtjoluu3xxf1qcjzlxbbyw0omgyqmju7atjznyyc0jpsmvypfrfc7rwizgekmsyzy1bkny0blxazq8yza2vs0rwoprloqjwpekh3nqycbrzbtijz3gff0y8z12l13svzqchhly8cvgpsmv8pl2gbyw0oerubqhyzy1bknywblgsonzyzs2vs0rdi1xyhmju7atjclgdx0jpsmvjog2g3eepin2t13svzlcd72tfxmrd7mcw7ljg70qyi02whlujoucd7medx3rzbtijz02hu0rdi880qqkwo23jknjdcc2kkmczpyxfm7qyiwgwctgfptchhnl0b18a7m8ypaxfh0qyokx713svzdtj3mj8c0jpsmv8plxgf0wvz880qqkdoehdqlr8v0jpsmv0pdrg37uui3rukqa0od3gbnf0318a7qojz8rf1wtfmireomkdz23kmmtfxmgfsqivpm2kc7uvm7jlzmgjp03bknyvc2xdbtijzugkowgdoux713svzrhdmllvcpgpsmvjzdggo7u8zt2qemspmy1bknywcmgd33czpy2hs0gjz3rlumk8pa3k72tfxmrktmz87frk70wpop2wzlk8patk72tfxm2jbmv0odgfuwado7jlzqfyz2hholq0318a7q28z12ho0svm7jlzqhdoacvorlvcz2a7l3jzd2gm0qyi7x713svzwtjoluu3xxfoncdoggjuetfmireolgypshfontfxmrdbmb0zljg70gjzogu1qu07ekh3ltvc1rzbtijzsrgfwhdmire3qgwpecfqnldx0jpsmvpolgfuwypp880qqkypehdcnty3xxf3l88zq2h70tjz7xw13svz3cfhljycqrzbtijzqrkbwtfmirekmsvo0tjhnqpcz2abqb17frkm7uvzp2qzqjwoe3bknywxqgj33czpy2kc0rdo880qqkwpktjsluu3xxfhmivp32vs0r0zb2lcna8pl3a72tfxm2kbmovp72jbyw0om2ybmuwzy1bknywxzxfkqb17frkh0wwiy20qqkwo23g7myjxu2acnczpy2kswwwiw2e3lg07ekh3nypcwrzbtijz72jm7qyokx713svzkhh3qhvccgammzpplxbbyw0omgyqmju7atj3meyxwrauqxyiygjzwtfmiretqsjoehd3mg8xyxftqb17frkk0u8zm2wolgwo7hdkluu3xxf7nz8p3xguwypp3gw7mju7atjfnljb12k7n2ypmrktwtfmire7mtdpy1bknywblgsonzyzs2vs0rdi1xyhmju7atjznyyc0jpsmvpolgfuwypp880qqkpoeckomyyc18a7q88z8rgbeg0o7ructgfptck3nt0318a7q28z12ho0svm7jlzqjwzg3f3qhy3xxf7nzdilrf38uui3rukqa0od3gbnfvc2xd33czpyxgf0jdorru7ldwotcg72tfxmxftmbppf2jh0uui3gebmupok3k1mtfxmgssm88z1xdu0wyi880qqkwpktjsluu3xxfkq10zt8vs0rpoiglemr8z03korlvcmxa1qo8z8rkuwwwo7jlzmkjp7hhznj0318a7q1ypyrhbyw0oz2wuqgfpttjhlldczxd33czpygdbwgpin2tctgfptchqnyyx0jpsmvvog2h70evm7jlzmajpuck3qhjxlgd7l3yis2j38uui3xl7lhwpkhh3mtpbyrzbtijz12jk0tjz7gukqjwpkhaoluu3xxfcmv8zt8vs0r8o1guoldyz2tdzmfwx1rzbtijzqrkbwtfmireolgwojtfcmr8x8rzbtijz02hswtfmire7lspoh3gzmtyx18a7mzppmgfuww0zbrezlgfptchhngyclgdmlb17frkk0udiogukluwo23kmnq8xxxd33czpyxgf0tjotxyemuyz3cfqqqyc0jpsmvjoy2kh0t8zr2w3qrvz03ghll0318a7mvvpfgdbwgjz7jlzqddp03kcntpb18a7mo8osxduwhjib2wolu07ekh3nedxqrj7mi8pu2hs0uvm7jlzmgvzecvorlvc7rdqm3jom2vs0r0zbgt1qu07ekh3lgyclgsom7w7ljg7wyvz7jlzmyyo3cgorlvcbgjtlb17frkowhdmirehqjpow3kkntwc0jpsmvpznrfb0uui3xyoldjzshhhnqycy2kumv87frko0kyi3xykqsdp3cvorlvc2gj7nc87frk1wujot2yoqgfpthkoqlvcygsfqijot8vs0rjoo2yomayzekh3lkybbrkbtijzexfbyw0oeglkmr07ekh3nedxqrj7mi8pu2hs0uvm7jlzmgvzecvorlvc2rk7mczpyxjb0rwizxuequ8p03ghll0318a7mzpp8xd70wdi22qqqg07ekh3ltvc1rzbtijzexf70uui3reemsdoehd3mejxxgpsmvwonrftwtfmiremmyvzekh3nyjxx2jbq8jogrgs0uui3gu1qajpn3jfluu3xxfuqijomrkf0e8obgl1mju7atjznyyc0jpsmvpz3rkbyw0o02tqqjypktjorlvc7raumxppt8vs0rpiiru3lkjpfhjqllyc0jpsmv0zy2j38uui3rwmns07ekh3nj8xbrkhmzpzxrko0yvm7jlzmajpm3k1mjjbzrj7qzw7ljg70gpizxutqddzb3bknydcwrzbtijzqrkbwtfmirekmsvo0tjhnqpcz2abqb17frkc0rdo880qqkwpsth7mtfxmxkumowos2ho0svm7jlzmuyzw3f3nty3xxfcmcwolggc0uyi32w13svzw3g3qh8cxxfbqb17frk1wg8z12t13svz8hdqlg0318a7q88zargo7udmire1nsjouhhzmtwc18a7mvyzsxg7etfmiretqs8zwtdzmlpb1xkcn70plxbbyw0orrltmay7atj7me0b1rauqi8zt8vs0rpiiru3lkjpfhjqllyc0jpsmv0zy2j38uui3xlkqa8p03bknyvcc2a7m3do12js0rpp880qqk8zahhhlh8cygdbtijza2jkwudoyx713svzkhh3mtpbygpsmvjoxgg70uui3ge1nhdzehd3quu3xxf1qippdxd1wkdo7jlzqk0puhhkntwx18a7qc8z32jswudoirekqgvoy1bknydcwxd1mzppqgd1wg0z880qqkvok3h7qq8x2gh7qxvzt8vs0rjiory1muvom3gzmy0x0jpsmv0pdrg37uui3rukqa0od3gbnf0318a7qovpdxfbyw0o3rlumk8pa3k72tfxm2kbqc8oy2jbyw0o02wclgdpw3kknyyc18a7qcyp8xd1ww0o7x713svz33gslkwxz2dunvpzwxbbyw0oprl7lgpo3tfqlhvcprzbtijzqrkbwtfmiremlgpokhkqquu3xxf7nvyp7xbbyw0ongw7ng07ekh3njwxrrzbtijzu2hc7uui32lbms8p8cvorlvcw2kbmczpyxgf0wvz7jlzmy8p83kfnedxz2azqb17frkm7udiogy1qgfptckoqkwxzxf1q38zljg7wu8omx713svzqhfkluu3xxfuqijomrkf0e8obgl1mju7atjznedxygd33czpy2kc0rdo880qqkvot3gbquu3xxfhmivp32vs0rvzbxu1ma8pa3gknr0318a7mzpp8xd70wdi22qqqg07ekh3ltvc1rzbtijz12jk0wyz720qqkwz7cdfnevc7rjbmczpyxjm0yvm7jlzqu8pfcdfnedcfxfomxw7ljg7wewiq2wolujokcf3le0318a7qcvpm2vs0r0onrw13svzdthhlky3xxf3nzwolxguwwpiirwctgfpttkolky3xxfhmivolgfuwwwo880qqk8patdzmyjxb2fuqi8zt8vs0r0zb2lcna8pl3a3lrjc0jpsmv0pdrdbwg0zq2q3lk0py1bknywcmgd33czpygj37uui3xqbmuwzehholty3xxf1mvjzn2g38uui3gwclgfptcgmljwc12abqc87frkc0wjz880qqk8patdzmyjxb2fuqi8zt8vs0r0zb2lcna8pl3a3lrjc0jpsmv0pdrdbwg0zq2q3lk0py1bknywcmgd33czpy2kcwldztxy13svz33gumtvb0jpsmvpolgfuwypp880qqkdzdthmlh8xxxd3niypfxd7ekppp2wctgfpttd7qq8xygpsmvjzh2kswwdopructgfptck3nt0318a7q28z12ho0svm7jlzqkpoe3jzmty3xxf7l3yzsxgbyw0ongu1qgy7atj3lqybqrjbq8jot8vs0r0z7xl1qkwoekh3ny0x7gssmczpyrg3ekvm7jlzmgvzecvorlvcz2a7l3jzd2gm0qyi7x713svzuckunhvbygpsmvjoggsb0gvm7jlzmtyz23gbnf0318a7q88zq2d70udmiremqjdps3fzquu3xxfmq88zrrhbyw0oz2wumgyz8cvorlvc8xfbqb17frkm7u0zo2yolkyzekh3nj0x72kuqivzt8vs0r0op2wbmsyo83bknyvbc2dcnczpy2h3etfmire3ndpos3fcle0burjbtijz3gfuwwjz880qqkdoehdqlr8v0jpsmvpz3rj1wjdor2qmqw07akc"
		
		if self.simulation:
			opt_command = "aocl-opt -translate-library-calls -reverse-library-translation -lowerconv -scalarize -scalarize-dont-touch-mem-ops -insert-ip-library-calls -createemulatorwrapper -emulator-channel-depth-model default  -generateemulatorsysdesc  -board "
			opt_command += "$AOCL_BOARD_PACKAGE_ROOT/hardware/pac_a10/board_spec.xml -dbg-info-enabled "
			opt_command += "Py2FPGAGenerated.1.bc" + " -o " + "Py2FPGAGenerated.bc"
			opt_command += ' >> ' + "Py2FPGAGenerated.log" + ' 2> ' + "opt.err"
		else:
			opt_command = "aocl-opt --acle " + acle_key + " -board $AOCL_BOARD_PACKAGE_ROOT/hardware/pac_a10/board_spec.xml "
			opt_command += "-dbg-info-enabled --grif --soft-elementary-math=false --fas=false --wiicm-disable=true "
			opt_command += "Py2FPGAGenerated.1.bc" + " -o " + "Py2FPGAGenerated.kwgid.bc"

		opt_command_2 = "aocl-opt -insert-ip-library-calls  -dbg-info-enabled --grif --soft-elementary-math=false --fas=false --wiicm-disable=true "
		opt_command_2 += '"Py2FPGAGenerated.kwgid.bc"' + " -o " + 'Py2FPGAGenerated.lowered.bc'

		link_command_2 = "aocl-link " + '"Py2FPGAGenerated.lowered.bc"' + " $ALTERAOCLSDKROOT/share/lib/acl/acl_late.bc "
		link_command_2 += '-o "Py2FPGAGenerated.linked.bc"'

		opt_command_3 = "aocl-opt -board $AOCL_BOARD_PACKAGE_ROOT/hardware/pac_a10/board_spec.xml    -always-inline -add-inline-tag -instcombine -adjust-sizes -dce -stripnk -rename-basic-blocks  -dbg-info-enabled --grif --soft-elementary-math=false --fas=false --wiicm-disable=true "
		opt_command_3 += '"Py2FPGAGenerated.linked.bc" -o "Py2FPGAGenerated.bc"'
		
		llc_command = "aocl-llc  -march=griffin  -board $AOCL_BOARD_PACKAGE_ROOT/hardware/pac_a10/board_spec.xml       -dbg-info-enabled "
		llc_command += '"Py2FPGAGenerated.bc" -o "Py2FPGAGenerated.v"'
		
		sys_integrator_command = "system_integrator   --bsp-flow green_top $AOCL_BOARD_PACKAGE_ROOT/hardware/pac_a10/board_spec.xml "
		sys_integrator_command += '"Py2FPGAGenerated.bc.xml" none kernel_system.tcl'


		
		gen_kernel_command = 'aocl-clang -fPIC -shared -Wl,-soname,libkernel.so -L"$ALTERAOCLSDKROOT/host/linux64/lib/" -lacl_emulator_kernel_rt -o ' + "libkernel.so" + ' -O0 ' + "Py2FPGAGenerated.bc" + ' >> ' + "Py2FPGAGenerated.log" + ' 2> ' + "opt.err"

		create_working_dir_command = "mkdir " + self.build_dir + "/Py2FPGAGenerated"
		# copy_board_files_command = "cp -r $AOCL_BOARD_PACKAGE_ROOT/hardware/pac_a10/* kernel/"
		copy_board_files_command = "cp -r $AOCL_BOARD_PACKAGE_ROOT/hardware/pac_a10/* ./"


		modify_qsf_files_command = 'for file in $(ls *.qsf)' + "\n"
		modify_qsf_files_command += 'do' + "\n"
		modify_qsf_files_command += '	echo "set_global_assignment -name SOURCE_TCL_SCRIPT_FILE ip_include.tcl" >> $file' + "\n"
		modify_qsf_files_command += '	echo "set_instance_assignment -name AUTO_SHIFT_REGISTER_RECOGNITION OFF -to *_NO_SHIFT_REG*" >> $file' + "\n"
		modify_qsf_files_command += '	echo "set_global_assignment -name VERILOG_CONSTANT_LOOP_LIMIT 10000" >> $file' + "\n"
		if self.fast_compile:
			modify_qsf_files_command += '	echo "set_global_assignment -name REPORT_PARAMETER_SETTINGS OFF" >> $file' + '\n'
			modify_qsf_files_command += '	echo "set_global_assignment -name REPORT_SOURCE_ASSIGNMENTS OFF" >> $file' + '\n'
			modify_qsf_files_command += '	echo "set_global_assignment -name REPORT_CONNECTIVITY_CHECKS OFF" >> $file' + '\n'
			modify_qsf_files_command += '	echo "set_global_assignment -name PLACEMENT_EFFORT_MULTIPLIER 0.25" >> $file' + '\n'
			modify_qsf_files_command += '	echo "set_global_assignment -name ROUTER_TIMING_OPTIMIZATION_LEVEL MINIMUM" >> $file' + '\n'
			modify_qsf_files_command += r'	echo "set_global_assignment -name PROGRAMMABLE_POWER_TECHNOLOGY_SETTING \"Force All Used Tiles to High Speed\"" >> $file' + '\n'
			modify_qsf_files_command += '	echo "set_global_assignment -name OPTIMIZE_MULTI_CORNER_TIMING OFF" >> $file' + '\n'
			modify_qsf_files_command += '	echo "set_global_assignment -name TIMEQUEST_MULTICORNER_ANALYSIS OFF" >> $file' + '\n'
			modify_qsf_files_command += '	echo "set_global_assignment -name FINAL_PLACEMENT_OPTIMIZATION NEVER" >> $file' + '\n'
			modify_qsf_files_command += '	echo "set_global_assignment -name FITTER_AGGRESSIVE_ROUTABILITY_OPTIMIZATION NEVER" >> $file' + '\n'
			modify_qsf_files_command += '	echo "set_global_assignment -name FIT_ONLY_ONE_ATTEMPT ON" >> $file' + '\n'
			modify_qsf_files_command += '	echo "set_global_assignment -name QII_AUTO_PACKED_REGISTERS OFF" >> $file' + '\n'
			modify_qsf_files_command += '	echo "set_global_assignment -name BLOCK_RAM_TO_MLAB_CELL_CONVERSION OFF" >> $file' + '\n'
			modify_qsf_files_command += r'	echo "set_global_assignment -name INI_VARS \"vpr_high_speed_low_power_tiles_optimization_slow_cold_corner=off\;apl_discrete_dp=off\;disable_pda_during_routing=on\;vpr_disable_global_router_for_cre=on\;vpr_sp_clk_frac_gband_mult=1.5\;fsac_disable_tborrow=on\;vpr_mco_fast_and_slow_cold_models=off\;vpr_run_prog_clk_skew_optimization=off\;vpr_disable_global_router=on\;vpr_sp_clk_abs_gband_mult=1.5\;vpr_guardband_multiplier_stage_mask=384\;vpr_disable_global_router_for_rr=on\;vpr_route_timing_tamer_max_num_convergences=1\;apl_dp=off\;apl_disable_timing=on\;\"" >> $file' + '\n'

		modify_qsf_files_command += 'done' + "\n"


		copy_board_file_command = "cp board_spec.xml Py2FPGAGenerated/"

		file = open(self.ll_to_aoco_scprit_path, 'w')
		file.write(create_working_dir_command + "\n")
		file.write(cd_command + "\n")
		file.write(add_cl_clang_to_path_command + "\n")
		file.write(add_libs_to_path + "\n")
		file.write(emit_llvm_bc_command + "\n")
		file.write(link_command + "\n")
		file.write(opt_command + "\n")
		
		if self.simulation:
			file.write(gen_kernel_command + "\n")
		else:
			file.write(opt_command_2 + "\n")
			file.write(link_command_2 + "\n")
			file.write(opt_command_3 + "\n")
			file.write(llc_command + "\n")
			file.write(sys_integrator_command + "\n")
			file.write(copy_board_files_command + "\n")
			file.write(modify_qsf_files_command + "\n")
			file.write(copy_board_file_command + "\n")

		file.close()

		return_code = call(["chmod", "u+x", self.ll_to_aoco_scprit_path])
		assert(return_code == 0)
		# Revome shell=True when code works
		return_code = call([self.ll_to_aoco_scprit_path], shell=True)
		assert(return_code == 0)


	def compile_aoco_to_aocx(self):
		self.aoco_kernel_file_path = path.join(self.build_dir, "Py2FPGAGenerated.aoco")
		self.aocx_kernel_file_path = path.join(self.build_dir, "Py2FPGAGenerated.aocx")
		# command = "mkdir " + path.join(self.build_dir, "kernel")
		# return_code = call([command], shell=True)
		command = "cp Py2FPGAGenerated/Py2FPGAGenerated.aoco . && aoc -v -v " + "Py2FPGAGenerated.aoco" + " -o " + "Py2FPGAGenerated.aocx" + "> /dev/null"
		return_code = call([command], shell=True)
		assert(return_code == 0)

		command = "rm Py2FPGAGenerated.aoco && cp Py2FPGAGenerated.aocx Py2FPGAGenerated/"
		# command = "rm Py2FPGAGenerated.aoco && mv Py2FPGAGenerated.aocx Py2FPGAGenerated/"
		return_code = call([command], shell=True)
		assert(return_code == 0)
	
	def get_llvm_ir(self, cl_ll_file_path):

		self.cl_to_ll_scprit_path = path.join(self.build_dir, 'cl_to_ll.sh')
		self.ll_file_path = cl_ll_file_path

		add_cl_clang_to_path_command = "export PATH=$PATH:$ALTERAOCLSDKROOT/linux64/bin"
		add_libs_to_path = "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ALTERAOCLSDKROOT/linux64/lib/dspba/linux64/"	
		
		if self.simulation:
			emit_llvm_command = "aocl-clang -cc1 -target-abi opencl -emit-llvm -mllvm -gen-efi-tb -Wuninitialized -triple x86_64-unknown-linux-gnu -mllvm -board -mllvm "
		else:
			emit_llvm_command = "aocl-clang -cc1 -O3 -emit-llvm -Wuninitialized -triple fpga64 -mllvm -board -mllvm "

		emit_llvm_command += "$AOCL_BOARD_PACKAGE_ROOT/hardware/pac_a10/board_spec.xml -DACL_BOARD_pac_a10=1 -DAOCL_BOARD_pac_a10=1 "
		emit_llvm_command += self.cl_file_path + " -o " + path.join(self.build_dir, cl_ll_file_path) + " -g"


		file = open(self.cl_to_ll_scprit_path, 'w')
		file.write(add_cl_clang_to_path_command + "\n")
		file.write(add_libs_to_path + "\n")
		file.write(emit_llvm_command + "\n")
		file.close()

		return_code = call(["chmod", "u+x", self.cl_to_ll_scprit_path])
		assert(return_code == 0)
		# Revome shell=True when code works
		return_code = call([self.cl_to_ll_scprit_path], shell=True)
		assert(return_code == 0)

	def generate_code(self):
		self.cl_file_path = path.join(self.build_dir,'cl_placeholder.cl')
		file = open(self.cl_file_path, 'w')
		
		val_and_name = []
		for i in range(len(self.args_list)):
			val_and_name.append((self.args_list[i], self.args_names[i]))

		file.write("__kernel void pyfunc(")

		for arg, name in val_and_name:
			c_type = get_c_type_from_numba_type(str(typeof(arg)))
			# file.write("const " + c_type + name + ", ")
			file.write("__global const " + c_type + "*restrict " + name + ", ")
			# file.write("__global const " + c_type + "" + name + ", ")

			if is_array(str(typeof(arg))):
				file.write("__global const char *restrict arg_" + name + "_0, ")
				file.write("__global const char *restrict arg_" + name + "_1, ")
				file.write("__global const long *restrict " + name + "_no_idea2, ")
				file.write("__global const long *restrict " + name + "_no_idea3, ")
				dim = array_dimensions(str(typeof(arg)))
				for i in range(dim):
					file.write("__global const long *restrict " + name + "_dim1_" + str(i) +" , ")
				for i in range(dim):
					file.write("__global const long *restrict " + name + "_dim2_" + str(i) +" , ")

		
		c_return_type = get_c_type_from_numba_type(self.return_type)
		file.write("__global " + c_return_type + "*restrict retptr")
		# file.write(c_return_type + "* retptr")
		file.write(")\n")
		file.write("{}")
		file.close()
