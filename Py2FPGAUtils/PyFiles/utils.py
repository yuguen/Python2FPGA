from numba import typeof
import ctypes

def array_dimensions(type):
	assert type.startswith("array") or type.startswith("readonly array")
	type = type[6:len(type)-1]
	index1 = type.find(",", 0, len(type))+2
	index2 = type.find(",", index1, len(type))-1
	type = type[index1:index2]
	return int(type)

def find_llvm_func(ll):
	funcs = []
	index = ll.find('define', 0, len(ll))
	while not index == -1:
		end = ll.find('}\n', index, len(ll))
		funcs.append(ll[index:end+1])
		index = ll.find('define', index+6, len(ll))
	for func in funcs:
		if(func.find('cpython',0, len(func)) == -1):
			return func
	raise ValueError("llvm func not found")

def get_array_type(type):
	type = type[6:len(type)]
	end = type.find(",", 0, len(type))
	type = type[0:end]
	return type

def get_numba_args_types(args_list):
	args_type_list = []
	for arg in args_list:
		args_type_list.append(typeof(arg))
	return tuple(args_type_list)

def get_c_printf_type(type):
	string = ""
	is_list = False
	if(type.startswith("reflected list")):
		type = type[15:len(type)]
		is_list=True
		raise ValueError("Lists not supported yet")

	if(is_array(type)):
		type = get_array_type(type)

	if(type == "boolean"):
		string += "%d "
	elif(type == "uint8" or type == "byte"):
		string += "%u "
	elif(type == "uint16"):
		string += "%u "
	elif(type == "uint32"):
		string += "%u "
	elif(type == "uint64"):
		string += "%lu "
	elif(type == "int8" or type == "char"):
		string += "%d "
	elif(type == "int16"):
		string += "%d "
	elif(type == "int32"):
		string += "%d "
	elif(type == "int64"):
		string += "%ld "
	elif(type == "float32"):
		string += "%1.100f "
	elif(type == "float64" or type == "double"):
		string += "%1.100f "
	elif(type == "complex64"):
		raise ValueError("I don't know what complex64 is")
		# string += "c8"
	elif(type == "complex128"):
		raise ValueError("I don't know what complex128 is")
		# string += "c16"
	else:
		print(type)
		raise ValueError("Unsupported type signature")

	if(is_list):
		string+="* "

	return string

def get_c_type_from_numba_type(type):
	string = ""
	type_is_array = False
	if(type.startswith("reflected list")):
		type = type[15:len(type)]
		raise ValueError("Lists not supported yet")

	if(type.startswith("readonly ")):
		type = type[9:len(type)]

	if is_array(type):
		type = type[5+1:len(type)] #+1 to remove "("
		end = type.find(',', 0, len(type))
		type = type[0:end]
		# print ('"' + type + '"')
		type_is_array = True



	if(type == "boolean"):
		string += "bool"
	elif(type == "uint8" or type == "byte"):
		string += "unsigned char"
	elif(type == "uint16"):
		string += "unsigned short"
	elif(type == "uint32"):
		string += "unsigned int"
	elif(type == "uint64"):
		string += "unsigned long"
	elif(type == "int8" or type == "char"):
		string += "char"
	elif(type == "int16"):
		string += "short "
	elif(type == "int32"):
		string += "int"
	elif(type == "int64"):
		string += "long"
	elif(type == "float32"):
		string += "float"
	elif(type == "float64" or type == "double"):
		string += "double"
	elif(type == "complex64"):
		raise ValueError("I don't know what complex64 is")
		# string += "c8"
	elif(type == "complex128"):
		raise ValueError("I don't know what complex128 is")
		# string += "c16"
	elif(type == "none"):
		string += "int"
		# Because we need a return type no matter what
	else:
		print(type)
		raise ValueError("Unsupported type signature")

	# if(type_is_array):
	# 	string+="* "
	
	return string

def get_ctypes_type_from_numba_type(type):
	
	if(type.startswith("reflected list")):
		type = type[15:len(type)]
		raise ValueError("Lists not supported yet")

	if(type.startswith("readonly ")):
		type = type[9:len(type)]

	if is_array(type):
		type = type[5+1:len(type)] #+1 to remove "("
		end = type.find(',', 0, len(type))
		type = type[0:end]

	if(type == "boolean"):
		return ctypes.c_bool
	elif(type == "uint8" or type == "byte"):
		return ctypes.c_ubyte
	elif(type == "uint16"):
		return ctypes.c_ushort
	elif(type == "uint32"):
		return ctypes.c_uint
	elif(type == "uint64"):
		return ctypes.c_ulong
	elif(type == "int8" or type == "char"):
		return ctypes.c_char
	elif(type == "int16"):
		return ctypes.c_short
	elif(type == "int32"):
		return ctypes.c_int
	elif(type == "int64"):
		return ctypes.c_long
	elif(type == "float32"):
		return ctypes.c_float
	elif(type == "float64" or type == "double"):
		return ctypes.c_double
	elif(type == "none"):
		return ctypes.c_int		
	elif(type == "complex64"):
		raise ValueError("I don't know what complex64 is")
		# string += "c8"
	elif(type == "complex128"):
		raise ValueError("I don't know what complex128 is")
		# string += "c16"
	else:
		print(type)
		raise ValueError("Unsupported type signature")


def get_llvm_type_from_numba_type(type):
	string = ""
	is_list = False
	if(type.startswith("reflected list")):
		type = type[15:len(type)]
		is_list=True

	if(type.startswith("readonly ")):
		type = type[9:len(type)]

	if(type == "boolean"):
		string += "bool "
	elif(type == "uint8" or type == "byte"):
		string += "i8 "
	elif(type == "uint16"):
		string += "i16 "
	elif(type == "uint32"):
		string += "i32 "
	elif(type == "uint64"):
		string += "i64 "
	elif(type == "int8" or type == "char"):
		string += "i8 "
	elif(type == "int16"):
		string += "i16 "
	elif(type == "int32"):
		string += "i32 "
	elif(type == "int64"):
		string += "i64 "
	elif(type == "float32"):
		string += "float "
	elif(type == "float64" or type == "double"):
		string += "double "
	elif(type == "complex64"):
		raise ValueError("I don't know what complex64 is")
		# string += "c8"
	elif(type == "complex128"):
		raise ValueError("I don't know what complex128 is")
		# string += "c16"
	else:
		print(type)
		raise ValueError("Unsupported type signature")

	# if(is_list):
	# 	string+="* "

	return string


def is_array(type):
	return type.startswith("array") or type.startswith("readonly array")

def llvm6_to_llvm3(ll_ir):
	# print(ll_ir)
	#TODO: Fix wrong cut when using a structure "{..., ...}, " -> "...}, " instead of ", "
	# getelementptr instruction
	current_index = 0
	current_index = ll_ir.find(' getelementptr ', current_index, len(ll_ir)) + 15
	while (current_index is not 14):
		if(ll_ir[current_index:current_index+9] == "inbounds "):
			current_index += 9
		
		bracket_index = ll_ir.find('{', current_index, len(ll_ir))
		comma_index = ll_ir.find(',', current_index, len(ll_ir))
		
		if (bracket_index != -1 and (bracket_index < comma_index)):
			closing_bracket = ll_ir.find('}', bracket_index, len(ll_ir))
			comma_index = ll_ir.find(',', closing_bracket, len(ll_ir))

		next_item = comma_index

		ll_ir = ll_ir[0:current_index] + ll_ir[next_item+1: len(ll_ir)]
		# current_index -= (next_item-current_index)
		current_index = ll_ir.find(' getelementptr ', current_index, len(ll_ir)) + 15

	# load instruction
	current_index = 0
	current_index = ll_ir.find(' load ', current_index, len(ll_ir)) + 6
	while (current_index is not 5):
		bracket_index = ll_ir.find('{', current_index, len(ll_ir))
		comma_index = ll_ir.find(',', current_index, len(ll_ir))
		
		if (bracket_index != -1 and (bracket_index < comma_index)):
			closing_bracket = ll_ir.find('}', bracket_index, len(ll_ir))
			comma_index = ll_ir.find(',', closing_bracket, len(ll_ir))

		next_item = comma_index
		# next_item = ll_ir.find(',', current_index, len(ll_ir)) + 1
		ll_ir = ll_ir[0:current_index] + ll_ir[next_item+1: len(ll_ir)]
		# current_index -= (next_item-current_index)
		current_index = ll_ir.find(' load ', current_index, len(ll_ir)) + 6

	return ll_ir


def parse_args_names(func_code):
	args_names = []
	start_args = func_code.find('(', 0, len(func_code))+1
	prev_start_args = start_args
	end_args = func_code.find(')', start_args, len(func_code))
	end_current_arg = 0
	while(not end_current_arg == -1):
		end_current_arg = func_code.find(',', start_args, end_args)
		if(not end_current_arg == -1):
			args_names.append(func_code[start_args:end_current_arg].replace(" ", ""))
		prev_start_args = start_args
		start_args = end_current_arg+1
	args_names.append(func_code[prev_start_args:end_args].replace(" ", ""))
	return args_names

def remove_NRT_calls(ll_ir):

	occur = ll_ir.find("NRT_incref", 0, len(ll_ir))
	while (occur != -1):
		start_line = ll_ir.rfind("\n", 0, occur) + 1
		end_line = ll_ir.find("\n", occur, len(ll_ir))
		ll_ir = ll_ir[0:start_line] + ll_ir[end_line:len(ll_ir)]
		occur = ll_ir.find("NRT_incref", 0, len(ll_ir))

	occur = ll_ir.find("NRT_decref", 0, len(ll_ir))
	while (occur != -1):
		start_line = ll_ir.rfind("\n", 0, occur) + 1
		end_line = ll_ir.find("\n", occur, len(ll_ir))
		ll_ir = ll_ir[0:start_line] + ll_ir[end_line:len(ll_ir)]
		occur = ll_ir.find("NRT_decref", 0, len(ll_ir))

	return ll_ir

def verify_addr_spaces(ll_ir):
	current_index = 0
	star = ll_ir.find('*', current_index, len(ll_ir))
	addrspace = ll_ir.find('addrspace(1)', current_index, len(ll_ir))
	current_index = star
	while (current_index != -1):
		if((addrspace != (star-12)) and (addrspace != (star-13))):
			ll_ir = ll_ir[0:star] + " addrspace(1)" + ll_ir[star:len(ll_ir)] 
			current_index += 13

		star = ll_ir.find('*', current_index+1, len(ll_ir))
		addrspace = ll_ir.find('addrspace(1)', current_index+1, len(ll_ir))
		current_index = star

	return ll_ir
