from os import path
from subprocess import call

class PerlScriptManager:
	
	def __init__(self, build_dir, script_name, simulation):
		self.build_dir = build_dir
		self.script_name = script_name
		self.simulation = simulation

	def generate_script(self):
		script = "require acl::Pkg;" + "\n"
		script += "\n"
		script += "sub save_pkg_section($$$) {" + "\n"
		script += "  my ($pkg,$section,$value) = @_;" + "\n"
		script += "  my $file = 'value.txt';" + "\n"
		script += r'  open(VALUE,">$file") or (print STDERR "Cannot open temporary file\n" and exit 1);' + "\n"
		script += "  binmode(VALUE);" + "\n"
		script += "  print VALUE $value;" + "\n"
		script += "  close VALUE;" + "\n"
		script += "  $pkg->set_file($section,$file)" + "\n"
		script += r'  or (print STDERR "Cannot save temporary file to package\n" and exit 1);' + "\n"
		script += "  unlink $file;" + "\n"
		script += "}" + "\n"
		script += "\n"
		
		script += 'my $pkg = create acl::Pkg("Py2FPGAGenerated.aoco");' + "\n"
		
		if self.simulation:
			script += "my $prebc = 'Py2FPGAGenerated.pre.bc';" + "\n"
			script += 'save_pkg_section($pkg, ".acl.clang_ir.x86_64-unknown-linux-gnu", $prebc);' + "\n"
		
		if self.simulation:
			script += "my $device = 'EmulatorDevice';" + "\n"
		else:
			script += "my $device = 'pac_a10';"
		script += 'save_pkg_section($pkg, ".acl.board", $device);' + "\n"

		script += "my $compileoptions = ' ';" + "\n"
		script += 'save_pkg_section($pkg, ".acl.compileoptions", $compileoptions);' + "\n"
		script += 'my $acl_version = "17.0.0.290";' + "\n"
		script += 'save_pkg_section($pkg, ".acl.version", $acl_version);' + "\n"

		if self.simulation:
			script += "$pkg->set_file('.acl.llvmir','Py2FPGAGenerated.bc');" + "\n"
		
		script += "$pkg->set_file('.acl.autodiscovery','sys_description.txt');" + "\n"

		if self.simulation:
			script += "$pkg->set_file('.acl.emulator_object.linux','libkernel.so');" + "\n"
		else:
			script += "$pkg->set_file('.acl.autodiscovery.xml','autodiscovery.xml');" + "\n"
			script += "$pkg->set_file('.acl.board_spec.xml','board_spec.xml');" + "\n"

		script += "$pkg->set_file('.acl.kernel_arg_info.xml','kernel_arg_info.xml');" + "\n"
		script += "\n"

		file = open(path.join(self.build_dir, self.script_name), 'w')
		file.write(script)
		file.close()

	def run_script(self):
		command = "cd " + self.build_dir + " && perl -I$ALTERAOCLSDKROOT/share/lib/perl " + self.script_name
		return_code = call([command], shell=True)
		assert(return_code == 0)