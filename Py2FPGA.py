from shutil import rmtree, which
from os import mkdir, path, environ
from inspect import getsource
from subprocess import call, check_output
from types import FunctionType
from ast import literal_eval
from ctypes import cdll, c_void_p
from time import time


from numba.targets.registry import CPUDispatcher
from numba.compiler import Pipeline
from numba import typeof
from numba.sigutils import normalize_signature

from Py2FPGAUtils.PyFiles.utils import *
from Py2FPGAUtils.PyFiles.hostCodeManager import HostCodeManager
from Py2FPGAUtils.PyFiles.clPlaceholderManager import CLPlaceholderManager

from numpy import array

class ExecutionFlow():

	def __init__(self, func, args_list, simulation, fast_compile):
		self.func = func
		self.args_list = args_list
		self.build_dir = "Py2FPGAGenerated"
		self.simulation = simulation
		self.fast_compile = fast_compile
		
		altera=True
		try:
			environ['ALTERAOCLSDKROOT']
		except KeyError:
			raise ValueError("ALTERAOCLSDKROOT is not set")

		self.args_names = parse_args_names(getsource(self.func))
		self.output_is_in_args = False
		for args in self.args_names:
			if(args.startswith('fpga_output_')):
				if(self.output_is_in_args):
					raise ValueError("Cannot have multiple fpga outputs as arguments.")
				else:
					self.output_is_in_args = True

	def clean(self):
		try:
			rmtree('__pycache__')
		except FileNotFoundError:
			pass

	def get_cl_placeholder_llvm_ir(self):
		self.cl_ll_file_path = path.join(self.build_dir,'cl_placeholder.ll')
		self.cl_placeholder_manager.get_llvm_ir('cl_placeholder.ll')

	def compile_host_code(self):
		self.shared_object_path = path.join(self.build_dir,'libhost.so')
		self.host_code_manager.compile(self.shared_object_path)

	def compile_modified_llvm_ir(self):
		self.cl_placeholder_manager.get_required_files(self.final_ll_name)
		self.cl_placeholder_manager.generate_aoco_file()
		self.cl_placeholder_manager.compile_aoco_to_aocx()

	def generate_cl_placeholder(self):
		self.cl_placeholder_manager = CLPlaceholderManager(self.build_dir, self.args_names, self.args_list, self.infered_return_type, self.simulation, self.fast_compile)		
		self.cl_placeholder_manager.generate_code()

	def generate_host_code(self):
		self.host_cpp_file_path = path.join(self.build_dir,'host.cpp')

		types_and_names_and_values = []

		for i in range(len(self.args_names)):
			types_and_names_and_values.append((self.args_type[i], self.args_names[i], self.args_list[i]))

		self.numpy_arrays = []
		self.numpy_arrays_values = []
		for arg_type, name, value in types_and_names_and_values:
			if(is_array(str(arg_type))):
				self.numpy_arrays.append(name)
				self.numpy_arrays_values.append(value)


		self.host_code_manager = HostCodeManager(self.host_cpp_file_path, self.args_names, self.args_type, self.args_list, self.infered_return_type, self.output_is_in_args, path.dirname(__file__), self.numpy_arrays, True) 
		self.host_code_manager.generate_code()

	def generate_numba_llvm_ir(self):
		self.ll_file_path = path.join(self.build_dir,'fpga_accelerated.ll')
		
		self.dispatcher = CPUDispatcher(py_func=self.func, locals={}, targetoptions={'nopython': True}, impl_kind='direct', pipeline_class=Pipeline)
		
		self.args_type = get_numba_args_types(self.args_list)
		self.dispatcher.compile(self.args_type)
		
		overload = self.dispatcher.overloads.popitem()
		self.dispatcher.overloads[overload[0]] = overload[1]
		self.infered_return_type = str(overload[1].signature.return_type)

		# dispatcher.overloads.push(overload)

		file = open(self.ll_file_path, 'w')
		ll = ''
		for k,v in self.dispatcher.inspect_llvm().items():
			ll = v
		file.write(ll)
		self.ll_func = find_llvm_func(ll)

		# print (ll)
		# Keeping only the core
		start = self.ll_func.find('{\n', 0)
		end = self.ll_func.find('}\n\n', start)
		self.ll_func = self.ll_func[start+1:end]

		# print(self.ll_func)
		file.close()

	def make_build_dir(self):
		try:
			mkdir(self.build_dir)
		except FileExistsError:
			rmtree(self.build_dir)
			mkdir(self.build_dir)

	def place_numba_ir(self):
		file = open(self.cl_ll_file_path, 'r')
		cl_ll_str = ""
		for line in file.read():
			cl_ll_str += line
		file.close()
		
		func_start_index = cl_ll_str.find("pyfunc", 0)
		func_start_index = cl_ll_str.find("{", func_start_index)+1
		func_end_index = cl_ll_str.find("\n}\n", func_start_index)

		before_numba = cl_ll_str[0:func_start_index]
		after_numba = cl_ll_str[func_end_index:len(cl_ll_str)]

		types_and_names = []
		for i in range(len(self.args_names)):
			types_and_names.append((self.args_type[i], self.args_names[i]))

		load_arguments = "\n"
		for arg_type, name in types_and_names:
			if is_array(str(arg_type)):
				# raise ValueError("Arrays not supported yet")
				load_arguments += "  %arg_" + name + "_2 = load i64 addrspace(1)* %" + name + "_no_idea2, !tbaa !20" + "\n"
				load_arguments += "  %arg_" + name + "_3 = load i64 addrspace(1)* %" + name + "_no_idea3, !tbaa !20" + "\n"
				for i in range(array_dimensions(str(arg_type))):
					load_arguments += "  %arg_" + name + "_5_" + str(i) + " = load i64 addrspace(1)* %" + name + "_dim1_" + str(i) + ", !tbaa !20" + "\n"
					load_arguments += "  %arg_" + name + "_6_" + str(i) + " = load i64 addrspace(1)* %" + name + "_dim2_" + str(i) + ", !tbaa !20" + "\n"
			else: 
				load_arguments += "  %arg." + name + " = load " + get_llvm_type_from_numba_type(str(arg_type)) + " addrspace(1)* %" + name + ", !tbaa !20" + "\n"
			self.ll_func = self.ll_func.replace("* %" + name, " addrspace(1)* %" + name)
			self.ll_func = self.ll_func.replace("arg." + name + ".4", name)
			self.ll_func = self.ll_func.replace("arg." + name + ".5.", "arg_" + name + "_5_")
			self.ll_func = self.ll_func.replace("arg." + name + ".6.", "arg_" + name + "_6_")
			self.ll_func = self.ll_func.replace("arg." + name + ".0", "arg_" + name + "_0")
			self.ll_func = self.ll_func.replace("arg." + name + ".1", "arg_" + name + "_1")
			self.ll_func = self.ll_func.replace("arg." + name + ".2", "arg_" + name + "_2")
			self.ll_func = self.ll_func.replace("arg." + name + ".3", "arg_" + name + "_3")
		

		load_arguments += "\n"

		#hacks
		self.ll_func = llvm6_to_llvm3(self.ll_func)
		self.ll_func = verify_addr_spaces(self.ll_func)
		self.ll_func = remove_NRT_calls(self.ll_func)


		numba_ll_entry_point = self.ll_func.find(":", 0)
		self.ll_func = self.ll_func[0:numba_ll_entry_point+1] + load_arguments + self.ll_func[numba_ll_entry_point+1:len(self.ll_func)]


		numba_ll_ret = self.ll_func.find("ret ", 0)
		while (numba_ll_ret != -1):
			numba_ll_ret_end_line = self.ll_func.find("\n", numba_ll_ret)
			numba_ll_ret_start_line = self.ll_func.rfind("\n", 0, numba_ll_ret) + 1
			self.ll_func = self.ll_func[0:numba_ll_ret_start_line] + "  ret void\n" + self.ll_func[numba_ll_ret_end_line:len(self.ll_func)] 
			numba_ll_ret = self.ll_func.find("ret ", numba_ll_ret_start_line+10)

		# tail_call = self.ll_func.find("tail ", 0)
		# while (tail_call != -1):

		# 	tail_call_end_line = self.ll_func.find("\n", tail_call)
		# 	tail_call_start_hashtag = self.ll_func.find("#", tail_call)
		# 	if((tail_call_start_hashtag < tail_call_end_line) and (tail_call_start_hashtag != -1)):
		# 		self.ll_func = self.ll_func[0:tail_call_start_hashtag] + self.ll_func[tail_call_end_line:len(self.ll_func)] 
		# 	tail_call = self.ll_func.find("tail ", tail_call+1)




		if(self.output_is_in_args):
			retptr_pos = self.ll_func.find("retptr", 0)
			while (retptr_pos != -1):
				retptr_end_line = self.ll_func.find("\n", retptr_pos)
				retptr_start_line = self.ll_func.rfind("\n", 0, retptr_pos) + 1
				self.ll_func = self.ll_func[0:retptr_start_line] + self.ll_func[retptr_end_line:len(self.ll_func)] 
				retptr_pos = self.ll_func.find("retptr", 0)
			excinfo_pos = self.ll_func.find("excinfo", 0)
			while (excinfo_pos != -1):
				excinfo_end_line = self.ll_func.find("\n", excinfo_pos)
				excinfo_start_line = self.ll_func.rfind("\n", 0, excinfo_pos) + 1
				self.ll_func = self.ll_func[0:excinfo_start_line] + self.ll_func[excinfo_end_line:len(self.ll_func)] 
				excinfo_pos = self.ll_func.find("excinfo", 0)


		# print(self.ll_func)

		self.final_ll = before_numba + self.ll_func + after_numba

		#Gros hack pour les benchs
		self.final_ll = self.final_ll + "\n" + "declare void @llvm.memset.p0i8.i64(i8 addrspace(1)*, i8, i64, i32, i1) " 
		self.final_ll = self.final_ll.replace("p0i8", "p1i8")
		

		self.final_ll_name = 'final_kernel.ll'

		self.final_ll_file_path = path.join(self.build_dir, self.final_ll_name)
		file = open(self.final_ll_file_path, 'w')
		file.write(self.final_ll)
		file.close()

	def program_FPGA(self):
		command = "aocl program acl0 " + self.build_dir + "/Py2FPGAGenerated.aocx"
		return_code = call([command], shell=True)
		assert(return_code == 0)

	def run_program(self):
		command = ""
		if self.simulation:
			environ["CL_CONTEXT_EMULATOR_DEVICE_ALTERA"]="1"
			# command += "env CL_CONTEXT_EMULATOR_DEVICE_ALTERA=1 "
		# command += self.build_dir + "/host"
		lib = cdll.LoadLibrary(self.shared_object_path)

		lib.my_func.restype = get_ctypes_type_from_numba_type(self.infered_return_type)
		fun = lib.my_func

		# import numpy
		# var = numpy.array([1,2], int)
		# var2 = numpy.array([3,4], int)
		formated_inputs = []
		for var in self.numpy_arrays_values:
			formated_inputs.append(c_void_p(var.ctypes.data))
		def wrapper(func, args):
			res = func(*args)
			return res

		raw_result = wrapper(fun, formated_inputs)
		# raw_result = check_output(command, shell=True)
		# raw_result = raw_result.decode("utf-8")
		if(not self.output_is_in_args):
			return raw_result
		else:
			return 0
		# 	return array(literal_eval(raw_result))
		# else:


def PyGA(func, args_list, simulation=True, fast_compile=False):
	assert(type(func)==FunctionType)
	assert(type(args_list)==list)
	
	flow = ExecutionFlow(func, args_list, simulation, fast_compile)

	start = time()
	# build_dir = "tempfiles"
	flow.make_build_dir()

	flow.generate_numba_llvm_ir()
	flow.generate_cl_placeholder()
	flow.get_cl_placeholder_llvm_ir()
	flow.place_numba_ir()
	flow.compile_modified_llvm_ir()
	end = time()
	print("(fpga) Compile target code: " + str(end - start) + " seconds.")
	
	start = time()
	flow.generate_host_code()
	flow.compile_host_code()
	end = time()
	print("(fpga) Compile host code: " + str(end - start) + " seconds.")

	if not simulation:
		start = time()
		flow.program_FPGA()
		end = time()
		print("(fpga) Program FPGA: " + str(end - start) + " seconds.")

	start = time()
	result = flow.run_program()
	end = time()
	print("(fpga) Run program: " + str(end - start) + " seconds.")
	
	flow.clean()

	return result


# res = subprocess.check_output(["ls", "-l"])
# res.decode("utf-8")
# x = ast.literal_eval(res)
