# Python2FPGA

This project aims at running a python function on an Intel Programmable Acceleration Card (PAC) with Arria 10 GX FPGA.

## Requirements

In order for this project to work on your platform you need to have installed:
- Python 3
- Numba
- Intel Acceleration Stack Development Package for the PAC
- Intel FPGA SDK for OpenCL

## Example returning a scalar 

Import the Py2FPGA library and call your function using  the `PyGA` function as illustrated as follow:
```
from Py2FPGA import *
def __my_func(a, b, n):
	res = 0
	for i in range(n):
		res+=(a*b)
	return res

res = PyGA(__my_func, [4, 5.3, 10])
```
Your function will then run on the FPGA.
Be aware that the compilation time is very long (at least an hour). You can accelerate the compilation by specifying `fast_compile=True` in the `PyGA` function.

In order to try your design you can use the simulation mode by passing `simulation=True` to the `PyGA` function.


## Example using numpy arrays

You can use and modify numpy arrays as long as you modify them item per item. 
The result must be returned through argument passing. 
Here is an example that loads a jpeg image and apply a filter to it :

```
from Py2FPGA import *
from PIL import Image
import numpy as np
import time

def load_image(infilename):
    img = Image.open( infilename )
    img.load()
    return img

def save_image(npdata, outfilename):
    img = Image.fromarray(np.uint8(npdata))
    img.save( outfilename )

def img_to_array(img):  
    data = np.asarray( img, dtype="uint8" )
    return data

def grayscale(input, width, height, fpga_output_img):
	for i in range(height):
		for j in range(width):
			gray_value = (input[i][j][0] * 0.299) + (input[i][j][1] * 0.587) + (input[i][j][2] * 0.114)
			fpga_output_img[i][j][0] = gray_value 
			fpga_output_img[i][j][1] = gray_value
			fpga_output_img[i][j][2] = gray_value
	return 0


img = load_image("image.jpg")
data = img_to_array(img)
width, height = img.size

result = np.zeros((height, width, 3), dtype="uint8")
PyGA(grayscale, [data, width, height, fpga_result], simulation=True, fast_compile=True)

save_image(fpga_result, "/tmp/result_image.jpg")

```

Using a dual socket motherboard with two 8 cores 16 threads Intel Xeon CPUs @ 2.60GHz, we observe the folowing results for the above example on a 12000x6000 pixels image are the following:
- Software time: 1495.4427118301392 seconds.
- (fpga) Compile target code: 5124.440534353256 seconds.
- (fpga) Compile host code: 0.668346643447876 seconds.
- (fpga) Program FPGA: 4.28839373588562 seconds.
- (fpga) Run program: 17.317359447479248 seconds.




Program succeed. 
(fpga) Compile target code: 8379.050123929977 seconds.
(fpga) Compile host code: 0.6671257019042969 seconds.
(fpga) Program FPGA: 4.30607271194458 seconds.
(fpga) Run program: 13.264548540115356 seconds.
Total FPGA time: 8397.298266172409 seconds.